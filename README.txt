$Id:

 * Overview
 * Credits

Overview
--------
An ApacheSolr helper module to index, query and retrieve attached images' file paths and ids of content nodes.
Benefits:
- You need not run resource intensive node_load() to get list of attached images for each items in results.
- Image paths / ids are available in apachesolr results array.
- Image path can be passed to imagecache preset
And this can be easily used in search results and other search screens such as themed "more like this" pages.

Checkout more at http://drupal.org/project/apachesolr_attached_images

Credits
-------
Vinay Yadav of VinayRas Infotech
http://www.vinayras.com